var express        =         require("express");
var app            =         express();
var util = require('util');

var bodyParser     =         require("body-parser");
//var request        =         require('request');

var sys            =         require('sys');
var exec           =         require('child_process').exec;


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/',function(req, res){
    res.sendfile("index.html");
});

app.post('/',function(req, res){

    function puts(error, stdout, stderr) { sys.puts(stdout) }

    var title = req.body.title;
    var content = req.body.content;
    var val = req.body.val;

    res.type('text/plain');
    res.writeHead(200, {'Content-Type': 'text/plain'});
    var tmp = "done";
    res.end(tmp);

    console.log("Messege: = "+ val );
	
	if(val == "SendMessage"){
	    console.log(val);
	    exec("../bash/box1.sh '"+title+"' '"+content+"'", puts);
	}	

	if(val == "CreateFile"){
	  console.log(val);
	  exec("../bash/CreateFile.sh");
	}

	if(val == "Lock"){
          console.log(val);
	  exec("../bash/lock.sh");
	}
});

app.listen(7474, function(){
    console.log("Started on PORT 7474");
});
